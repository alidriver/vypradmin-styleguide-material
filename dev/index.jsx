import React, {PureComponent, PropTypes} from 'react';
//import React from "react";
import ReactDOM from "react-dom";

import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog';
import {deepOrange500} from 'material-ui/styles/colors';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
};

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

var HelloWorld = React.createClass({
  render: function() {
    return (
      <p>Hello, {this.props.greetTarget}!</p>
    );
  }
});
 
class SteerCreationHeader extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {date: new Date()};
  }

  render() {
    return (
      <div>
        <h1 className="mdc-typography--display1">Steer creation</h1>
        <h2 className="mdc-typography--display2">Hello, Material Components!</h2>
        <h2>It is {this.props.pageTitle}.</h2>
      </div>
    );
  }
}

ReactDOM.render(
      <MuiThemeProvider muiTheme={muiTheme}>
        <div style={styles.container}>
          <SteerCreationHeader pageTitle="Ali finds react hard"/>
          <RaisedButton
            label="Super Secret Password"
            secondary={true}
          />
        </div>

      </MuiThemeProvider>
  ,
  document.querySelector("#container")
);